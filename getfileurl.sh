#!/bin/bash

FILENAME="/dev/shm/$(basename -s .sh $0).pdf"
# Main loop
while true; do
    # Prompt user for URL
	N=$(zenity --entry --title="Load URL" --text="Enter pdf URL")
    
    # Exit if user cancels
    [[ -z "$N" ]] && exit

    # run all process 
	curl -L "$N" --output "$FILENAME" && sync && sleep 1 && evince $FILENAME && rm -f $FILENAME && sync 
    # Display results using a text-info dialog
    echo -e "$results_string" | zenity --text-info --title="Done" --width=200 --height=250 --ok-label="Another URL ?" --cancel-label="Quit"

    # Check if user wants to quit
    if [ $? != 0 ]; then
        exit
    fi
done

