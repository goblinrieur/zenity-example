# zenity-example


# Just a very simple useless demonstration for a friend

This is just an how to , for methods how to call zenity in bash to display graphical GUI.
This example just roll N dices and dispay results. Nothing more.

# example pictures

![1](./1.png)

![2](./2.png)

![3](./3.png)

# code

```bash
#!/bin/bash

# Function to roll dice
roll_dice() {
    local -n dice_results=$1
    local dice_count=$2
    dice_results=() # Initialize array

    for ((i = 0; i < dice_count; i++)); do
        # Roll a die and add its result to the array
        dice_results+=("$(($RANDOM % 6 + 1))")
    done
}

# Main loop
while true; do
    # Prompt user for number of dice to roll
    N=$(zenity --entry --title="Roll Dice" --text="Enter number of dice to roll:")
    
    # Exit if user cancels
    [[ -z "$N" ]] && exit

    # Validate input is a number
    if ! [[ "$N" =~ ^[0-9]+$ ]]; then
        zenity --error --text="Please enter a valid number."
        continue
    fi

    # Roll dice
    declare -a results
    roll_dice results $N

    # Format results for display
    results_string=""
    for i in "${!results[@]}"; do
        results_string+="Dice $(($i + 1)): ${results[$i]}\n"
    done

    # Display results using a text-info dialog
    echo -e "$results_string" | zenity --text-info --title="Dice Results" --width=200 --height=250 --ok-label="Reroll" --cancel-label="Quit"

    # Check if user wants to quit
    if [ $? != 0 ]; then
        exit
    fi
done
```
